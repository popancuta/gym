package com.sda.gym.service;


import com.sda.gym.dao.ClassDao;
import com.sda.gym.model.Clas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassServiceImpl implements ClassService {

    @Autowired
    private ClassDao classDao;

    @Override
    public void save(Clas classId) {
        classDao.save(classId);
    }

    public Clas findByClassId(Integer classId){

       return  classDao.findById(classId).get();
    }


    @Override
    public List<Clas> getAllClasList() {
        return classDao.findAll();
    }
}

