package com.sda.gym.service;

public interface SecurityService {

        String findLoggedInUsername();

        void autoLogin(String username, String password);
    }
