package com.sda.gym.service;

import com.sda.gym.dao.RoleDao;
import com.sda.gym.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
     @Autowired
    private RoleDao roleDao;

    @Override
    public void save(Role role) {
        roleDao.save(role);
    }

    @Override
    public Role findByRoleId(Integer roleId) {
        return roleDao.findByRoleId(roleId);
    }

    @Override
    public List<Role> getAllRoles() {
        List<Role> roles = roleDao.findAll();
        return roles;
    }
}
