package com.sda.gym.service;

import com.sda.gym.model.Clas;

import java.util.List;

public interface ClassService {
    void save (Clas classId);

    Clas findByClassId(Integer classId);

    List<Clas>  getAllClasList();
}
