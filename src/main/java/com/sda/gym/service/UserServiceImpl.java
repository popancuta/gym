package com.sda.gym.service;

import com.sda.gym.dao.UserDao;
import com.sda.gym.model.Role;
import com.sda.gym.model.User;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public User findByUserId(Integer user) {
        return userDao.findByUserId(user);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = userDao.findAll();
        return users;
    }
}
