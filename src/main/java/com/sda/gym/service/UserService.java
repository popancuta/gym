package com.sda.gym.service;

import com.sda.gym.model.User;

import java.util.List;

public interface UserService {

    void save(User user);

    User findByUserId(Integer userId);

    List<User> getAllUsers();
}
