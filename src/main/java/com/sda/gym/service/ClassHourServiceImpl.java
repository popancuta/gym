package com.sda.gym.service;


import com.sda.gym.dao.ClassHourDao;
import com.sda.gym.model.ClassHour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public  class ClassHourServiceImpl implements ClassHourService {

    @Autowired
    private ClassHourDao classHourDao;


    @Override
    public void save(ClassHour classHour) {
        classHourDao.save(classHour);
    }

    @Override
    public ClassHourService findByClassId(Integer classId) {
        return null;
    }

    public void setClassHourDao(ClassHourDao classHourDao) {
        this.classHourDao = classHourDao;
    }

    @Override
    public List<ClassHour> getAllClassHourList() {
          return classHourDao.findAll();

    }
}
