package com.sda.gym.service;

import com.sda.gym.model.Role;

import java.util.List;


public interface RoleService {
    void save(Role role);

    Role findByRoleId(Integer roleId);

    List<Role> getAllRoles();

}
