package com.sda.gym.service;

import com.sda.gym.model.ClassHour;

import java.util.List;

public interface ClassHourService {

    void save (ClassHour classHour);

    ClassHourService findByClassId(Integer classId);

    List<ClassHour>  getAllClassHourList();
}
