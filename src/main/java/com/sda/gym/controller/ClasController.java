package com.sda.gym.controller;


import com.sda.gym.model.Clas;
import com.sda.gym.model.ClassHour;
import com.sda.gym.service.ClassHourService;
import com.sda.gym.service.ClassService;
import jdk.internal.dynalink.support.ClassMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/clas")

public class ClasController {

    @Autowired
    private ClassService classService;

    @Autowired
    private ClassHourService classHourService;

    @RequestMapping("/list")
    public String listClas(ModelMap model) {
        List<Clas> clasList = classService.getAllClasList();
        List<ClassHour> classHourList = classHourService.getAllClassHourList();
        System.out.println("size for clasList" + clasList.size());

        model.addAttribute("clasList", clasList);
        model.addAttribute("clasHourList", classHourList);

        return "clasListView";
    }
}

