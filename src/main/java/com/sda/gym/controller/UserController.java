package com.sda.gym.controller;

import com.sda.gym.model.Role;
import com.sda.gym.model.User;
import com.sda.gym.service.RoleService;
import com.sda.gym.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public String listUsers(ModelMap model) {
        List<User> users = userService.getAllUsers();
        List<Role> roles = roleService.getAllRoles();

        model.addAttribute("usersList", users);

        model.addAttribute("rolesList", roles);

        return "usersListView";

    }

//    @RequestMapping(value = "/delete", method = RequestMethod.POST)
//    public String delete(ModelMap model, @RequestParam("ClassHourId") Long ClassHourId)  {
//        User user = userService.getAllUsers()
//
//        Long currentUserId = userService.findByUsername(securityService.findLoggedInUsername()).getUserId();
//        // deleting the project from database
//        projectService.deleteById(projectId);
////        // this is the name of the attribute that will be iterated in the jsp file
//        // this is the name of the jsp file that will be used to display the page
//        List<Project> projects = projectService.findAllByUserId(currentUserId);
//        model.addAttribute("projectUserList" , projects);
//
//        return "projectsByUserId";
//    }


}

