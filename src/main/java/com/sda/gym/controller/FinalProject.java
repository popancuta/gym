package com.sda.gym.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.sda.gym.model","com.sda.gym.controller","com.sda.gym.service","com.sda.gym.dao"})

public class FinalProject extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FinalProject.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(FinalProject.class, args);
    }

}