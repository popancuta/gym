package com.sda.gym.controller;

import com.sda.gym.model.Clas;
import com.sda.gym.model.ClassHour;
import com.sda.gym.service.ClassHourService;
import com.sda.gym.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/clasHour")
public class ClasHourController {


        @Autowired
        private ClassService classService;

        @Autowired
        private ClassHourService classHourService;

        @RequestMapping("/list")
        public String listClasHour(ModelMap model) {

            List<ClassHour> classHourList = classHourService.getAllClassHourList();

            model.addAttribute("clasHourList", classHourList);

            return "clasHourListView";
        }
    }

