package com.sda.gym;

import com.sda.gym.model.Clas;
import com.sda.gym.model.ClassHour;
import com.sda.gym.service.ClassHourService;
import com.sda.gym.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class GymApplication implements CommandLineRunner {

    @Autowired
    private ClassService classService;

    @Autowired
    private ClassHourService classHourService;

    public static void main(String[] args) {
        SpringApplication.run(GymApplication.class, args);

    }
    @Override
    public void run(String...args) throws Exception {
        Date startDate = Calendar.getInstance().getTime();
        Date endDate = Calendar.getInstance().getTime();
        /*classHourService.save(new ClassHour(startDate, endDate));
        System.out.println(classService.findByClassId(2).getTrainerName());
        classService.save(new Clas("Aerobic"));*/

        Set<ClassHour> classHourSet = new HashSet<>();
        ClassHour classHour = new ClassHour(startDate, endDate);
        classHourSet.add(classHour);
        Clas clas = new Clas("Zumba");
        classHour.setClas(clas);


        clas.setClassHour(classHourSet);
        classService.save(clas);

        }



}
