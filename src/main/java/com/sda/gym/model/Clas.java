package com.sda.gym.model;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="class")
public class Clas {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CLASS_ID")
    private int classId;
    private String type;
    private String description;
    @Column(name = "TRAINER_NAME")
    private String trainerName;

    public Clas(){

    }

    public Clas(String type) {
        this.type = type;
    }

    public void setClassHour(Set<ClassHour> classHour) {
        this.classHour = classHour;
    }

    public Clas (String type, String description, String trainerName) {
        this.type = type;
        this.description = description;
        this.trainerName = trainerName;


    }

    @OneToMany(mappedBy = "clas", cascade = { CascadeType.ALL })
    private Set<ClassHour> classHour;



    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }


    }


