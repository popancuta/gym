package com.sda.gym.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table (name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "ROLE_ID")
    private int roleId;
    @Column (name = "ROLE_NAME")
    private String roleName;

    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role(String roleName) {
        this.roleName = roleName;
    }
    public Role(){

    }

    public Role(String roleName, Set<User> users) {
        this.roleName = roleName;
        this.users = users;
    }
}
