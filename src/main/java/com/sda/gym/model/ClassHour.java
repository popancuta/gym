package com.sda.gym.model;



import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static jdk.nashorn.internal.objects.NativeString.substring;

@Entity
@Table(name="CLASS_HOUR")
public class ClassHour {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CLASS_HOUR_ID")
    private int classHourId;


    public String getStartDatePrettyHour(){
        String   stringResult = startDate.toString().substring(0,startDate.toString().length()-5);
        return stringResult;
    }

      public String getEndDatePrettyHour(){
        String stringSecondResult =endDate.toString().substring(0,endDate.toString().length()-7);
        return  stringSecondResult;
      }



    @Column(name = "START_HOUR")
    private Date startDate;
    @Column(name = "END_HOUR")
    private Date endDate;

    @ManyToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name="CLASS_ID",nullable=false)
    private Clas clas;


    @ManyToMany(mappedBy = "classHours")
    private Set<User> users;

    public ClassHour(){

    }


    public int getClassHourId() {
        return classHourId;
    }

    public ClassHour(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ClassHour(int classHourId){
        this.classHourId= classHourId;
    }

    public void setClas(Clas clas) {
        this.clas = clas;
    }

    public void setClassHourId(int classHourId) {
        this.classHourId = classHourId;
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Clas getClas() {
        return clas;
    }
}
