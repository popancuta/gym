package com.sda.gym.dao;

import com.sda.gym.model.Role;
import com.sda.gym.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {
    Role findByRoleId(Integer RoleId);
}
