package com.sda.gym.dao;

import com.sda.gym.model.ClassHour;
import com.sda.gym.service.ClassHourService;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClassHourDao extends JpaRepository<ClassHour,Integer> {

}
