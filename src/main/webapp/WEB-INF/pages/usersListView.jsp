<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>

<h2>View all Users</h2>
<table border="1">
    <tr>
        <td>UserID</td>
        <td>User Name</td>
        <td>Password</td>
        <td>Email</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Status</td>
    </tr>
    <c:forEach items="${usersList}" var="user">
        <tr>
            <td>${user.getUserId() }</td>
            <td>${user.getUserName() }</td>
            <td>${user.getPassword() }</td>
            <td>${user.getEmail() }</td>
            <td>${user.getFirstName() }</td>
            <td>${user.getLastName() }</td>
            <td>${user.getStatus() }</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
