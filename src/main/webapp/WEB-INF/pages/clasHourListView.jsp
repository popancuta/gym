<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>--%>

<html>
<body>

<h2>View Class Hour</h2>
<body bgcolor="MediumSeaGreen">
<table border="1">
    <tr>
        <td>Class Name</td>
        <td>Trainer Name</td>
        <td>Start Hour</td>
        <td>End Hour</td>
        <td>Action</td>
    </tr>

    <c:forEach items="${clasHourList}" var="clasHour">
    <tr>
        <td>${clasHour.getClas().getType()}</td>
        <td>${clasHour.getClas().getTrainerName()}</td>
        <td>${clasHour.getStartDatePrettyHour()}</td>
        <td>${clasHour.getEndDatePrettyHour()}</td>
        <td><button onclick="myFunction()">Book</button></td>
    </tr>
    </c:forEach>
    <script>
        function myFunction() {
            alert("You booked your class!");
        }
    </script>

    <script type="text/javascript">
        document.getElementById("myButton").onclick = function () {
            location.href = "http://localhost:8080/clas/list";
        };
    </script>

    <div>
        <div>
            <h1></h1>
            <h2>${message}</h2>

            Press  <strong><a href="/clas/list"><button id="Button" class="float-left submit-button" >Here</button>
        </a></strong> to view previous page.
        </div>
    </div>

</body>
</html>
