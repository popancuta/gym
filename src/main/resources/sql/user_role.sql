CREATE TABLE `user_role` (
  `USER_ROLE_ID` int(11) NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ROLE_ID`),
  KEY `user_role_user_fk_idx` (`USER_ID`),
  KEY `user_role_role_fk_idx` (`ROLE_ID`),
  CONSTRAINT `user_role_role_fk` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`),
  CONSTRAINT `user_role_user_fk` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
)