CREATE TABLE `user_attending` (
  `USER_ATTENDING_ID` int(11) NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CLASS_HOUR_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`USER_ATTENDING_ID`),
  KEY `user_attending_user_fk_idx` (`USER_ID`),
  KEY `user_attending_class_hour_fk_idx` (`CLASS_HOUR_ID`),
  CONSTRAINT `user_attending_class_hour_fk` FOREIGN KEY (`CLASS_HOUR_ID`) REFERENCES `class_hour` (`CLASS_HOUR_ID`),
  CONSTRAINT `user_attending_user_fk` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`)
)