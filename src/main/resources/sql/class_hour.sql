CREATE TABLE `class_hour` (
  `CLASS_HOUR_ID` int(11) NOT NULL,
  `CLASS_ID` int(11) DEFAULT NULL,
  `START_HOUR` datetime(6) DEFAULT NULL,
  `END_HOUR` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`CLASS_HOUR_ID`),
  KEY `cass_hour_class_fk_idx` (`CLASS_ID`),
  CONSTRAINT `class_hour_class_fk` FOREIGN KEY (`CLASS_ID`) REFERENCES `class` (`CLASS_ID`)
)